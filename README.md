# NodeJS

## Crypt32

This only runs on Node 8.0.0 due to [this issue](https://github.com/node-ffi/node-ffi/issues/468).

`index.ts`:

```typescript
import * as fs from 'fs';
import * as ref from 'ref';
import * as ffi from 'ffi';
import * as Struct from 'ref-struct';

const DATA_BLOB = Struct({
    cbData: ref.types.uint32,
    pbData: ref.refType(ref.types.byte)
});
const PDATA_BLOB = ref.refType(DATA_BLOB);
const Crypto = ffi.Library('Crypt32', {
    "CryptUnprotectData": ['bool', [PDATA_BLOB, 'string', 'string', 'void *', 'string', 'int', PDATA_BLOB]],
    "CryptProtectData": ['bool', [PDATA_BLOB, 'string', 'string', 'void *', 'string', 'int', PDATA_BLOB]]
});

function encrypt(plaintext: string): string {
    const buf = Buffer.from(plaintext, 'utf16le');
    const dataBlobInput = new DATA_BLOB();
    dataBlobInput.pbData = buf;
    dataBlobInput.cbData = buf.length;
    const dataBlobOutput = ref.alloc(DATA_BLOB);
    const result = Crypto.CryptProtectData(dataBlobInput.ref(), null, null, null, null, 0, dataBlobOutput);
    const outputDeref = dataBlobOutput.deref();
    const ciphertext = ref.reinterpret(outputDeref.pbData, outputDeref.cbData, 0);
    return ciphertext.toString('base64');
};

function decrypt(ciphertext) {
    const buf = Buffer.from(ciphertext, 'base64');
    const dataBlobInput = new DATA_BLOB();
    dataBlobInput.pbData = buf;
    dataBlobInput.cbData = buf.length;
    const dataBlobOutput = ref.alloc(DATA_BLOB);
    const result = Crypto.CryptUnprotectData(dataBlobInput.ref(), null, null, null, null, 0, dataBlobOutput);
    const outputDeref = dataBlobOutput.deref();
    const plaintext = ref.reinterpret(outputDeref.pbData, outputDeref.cbData, 0);
    return plaintext.toString('utf16le');
};

const text = "åäöÅÄÖ_éÉóÓ D25P";
const ciphertext = encrypt(text);
const plaintext = decrypt(ciphertext);

console.log("text:", text);
console.log("ciphertext:", ciphertext);
console.log("plaintext:", plaintext);
```

`package.json`:

```json
{
  "main": "index.ts",
  "license": "MIT",
  "scripts": {
    "start": "ts-node index.ts"
  },
  "dependencies": {
    "@types/ffi": "^0.1.0",
    "@types/node": "^10.0.0",
    "@types/ref": "^0.0.28",
    "@types/ref-struct": "^0.0.29",
    "ffi": "^2.2.0",
    "ref": "^1.3.5",
    "ref-struct": "^1.1.0"
  }
}
```

[Source](https://github.com/node-ffi/node-ffi/issues/355#issuecomment-338391498)
